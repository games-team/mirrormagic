Source: mirrormagic
Section: games
Priority: optional
Maintainer: Drew Parsons <dparsons@debian.org>
Build-Depends: debhelper-compat (= 13),
 libsdl2-dev,
 libsdl2-image-dev,
 libsdl2-mixer-dev,
 libsdl2-net-dev
Rules-Requires-Root: binary-targets
Standards-Version: 4.7.0
Homepage: http://www.artsoft.org/mirrormagic/
Vcs-Git: https://salsa.debian.org/games-team/mirrormagic.git
Vcs-Browser: https://salsa.debian.org/games-team/mirrormagic

Package: mirrormagic
Architecture: any
Depends: mirrormagic-data, ${shlibs:Depends}, ${misc:Depends}
Conflicts: suidmanager (<< 0.50)
Description: Shoot around obstacles to collect energy using your beam.
 A game like "Deflektor" (C 64) or "Mindbender" (Amiga).
 The goal is to work out how to get around obstacles to shoot
 energy containers with your beam, enabling the path to the
 next level to be opened. Included are many levels known from
 the games "Deflektor" and "Mindbender".
 .
 Some features:
        - stereo sound effects and music
        - music module support for SDL version (Unix/Win32)
        - fullscreen support for SDL version (Unix/Win32)
        - complete source code included under GNU GPL

Package: mirrormagic-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Conflicts: suidmanager (<< 0.50)
Description: Data files for mirrormagic
 This package provides the architecture-independent data files (images,
 levels, sound samples) for mirrormagic.

